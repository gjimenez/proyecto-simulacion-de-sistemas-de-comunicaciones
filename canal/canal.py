import random
from random import randint

def codificadorCanal(string_bits):
    """ Codificador de canal"""
  
    secuencia = list(string_bits)
    secuencia = [int(bit) for bit in secuencia]

    #En salidaCodificador se tendrá el resultado final cuando se termine la codificación de todos los conjuntos de k bits
    salidaCodificador = []
    entradaCodificador = []

    #Define la matriz generadora G
    G = [[1, 0, 0, 0, 1, 1, 0],
        [0, 1, 0, 0, 1, 0, 1],
        [0, 0, 1, 0, 0, 1, 1],
        [0, 0, 0, 1, 1, 1, 1]]

    #Límites para recorrer el arreglo
    n = 0
    m = 4

    #Guarda la cantidad de ceros agregados en la secuencia en caso de que no sea múltiplo de k = 4
    ceros = 0

    #Recorre todo el arreglo de entrada
    while n < len(secuencia):

        #Se obtienen los k = 4 bits para codificar
        entradaCodificador = secuencia[n:m]

        #Si el tamaño es menor a k (pasa solo con el último conjunto enviado) le pone ceros hasta que el tamaño sea k
        while len(entradaCodificador) < 4:
            ceros = ceros + 1
            entradaCodificador = entradaCodificador.append(0)

        #Realiza la multiplicación del vector de entrada con la matriz G y se lo adjunta al resultado final
        
        # Codificacion de 7 bits saliente segun la secuencia de 4 bits
        resultado = [0,0,0,0,0,0,0]

        # Multiplicacion matricial entre la secuencia de 4 bits y G
        for i in range(4):
            for j in range(7):
                resultado[j] += entradaCodificador[i] * G[i][j]

        # La codificacion es binaria
        resultado = [valor%2 for valor in resultado]

   
        # Se adjunta al resultado final
        salidaCodificador.extend(resultado)
       

        #Suma 4 a los límites para la siguiente iteración
        n += 4
        m += 4


    return (salidaCodificador,ceros)

def decodificadorCanal(salidaCodificadorCanal):
    """ Decodificador de canal"""

    #Se define la matriz H
    H = [[1, 1, 0],
        [1, 0, 1], 
        [0, 1, 1],
        [1, 1, 1],
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]]

    n = 0
    m = 7

    sindromes = [0,0,0]
    entradaDecodificador = []
    salidaDecodificador = []

    #Recorre todo el arreglo de entrada del decodificador
    while n < len(salidaCodificadorCanal):

        #Se obtienen los n=7 bits para codificar
        entradaDecodificador = salidaCodificadorCanal[n:m]


        #Realiza la multiplicación del vector de entrada con la matriz H para obtener el sindrome
        # Multiplicacion matricial entre la secuencia de 4 bits y G
        for i in range(7):
            for j in range(3):
                sindromes[j] += entradaDecodificador[i] * H[i][j]

        # La decodificacion es binaria
        sindromes = [sind%2 for sind in sindromes]

        # Correcion segun el sindrome
        for i in range(7): #
            if H[i]==sindromes:
                if entradaDecodificador[i]==1:
                    entradaDecodificador[i]=0
                else:
                    entradaDecodificador[i]=1
       
        # Toma los ultimos 4 bits segun la matriz G
        salidaDecodificador.extend(entradaDecodificador[0:4])

        #Suma 7 a los límites para la siguiente iteración
        n = n + 7
        m = m + 7

    # El decoder de fuente recibe un string :(
    salidaDecodificadorStr = ''.join(str(bit) for bit in salidaDecodificador)

    return salidaDecodificadorStr

def BSC(entrada_bits, probabilidad):
    """ Simulacion de BSC """ 
    salida_bits = entrada_bits

    for i in range(len(entrada_bits)):
        if random.random() < probabilidad:
            if entrada_bits[i] == 1:
                salida_bits[i] = 0
            else:
                salida_bits[i] = 1

    return salida_bits




def main():
    """Función principal del programa
    """
    
    # Secuencia que se pasará por el codificador de canal
    secuencia = []

    #Crea una lista de números aleatorios para que sean la entrada del codificador de canal
    for i in range(0,16):
         a = randint(0,1)
         secuencia.append(a)

    print("La secuencia es: \t\t\t\t", secuencia)

    # Codificador de canal
    (salidaCodificadorCanal,cantidadCeros) = codificadorCanal(secuencia)

    print("La salida del codificador de canal es: \t\t", salidaCodificadorCanal)

    #Canal ideal
    salidaCanal = salidaCodificadorCanal

    # Decodificador de canal
    salidaDecodificadorCanalIdeal = decodificadorCanal(salidaCanal)

    # BSC (comentar si quiere un canal ideal)
    salidaCanal = BSC(salidaCodificadorCanal, 0.01)

    # Decodificador de canal
    salidaDecodificadorCanalBSC = decodificadorCanal(salidaCanal)

    salidaDecodificadorCanalIdeal = list(salidaDecodificadorCanalIdeal)
    salidaDecodificadorCanalIdeal = [int(bit) for bit in salidaDecodificadorCanalIdeal]

    print("La salida del decodificador de canal ideal es: \t", salidaDecodificadorCanalIdeal)

    salidaDecodificadorCanalBSC = list(salidaDecodificadorCanalBSC)
    salidaDecodificadorCanalBSC = [int(bit) for bit in salidaDecodificadorCanalBSC]

    print("La salida del decodificador de canal BSC es: \t", salidaDecodificadorCanalBSC)



if __name__ == "__main__":
    main()
