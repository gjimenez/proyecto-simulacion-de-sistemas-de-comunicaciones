# Proyecto Simulacion de Sistemas de Comunicaciones

Programa en Python (Python 3) que simula un sistema de comunicaciones completo con:
* Codec de fuente
* Codec de canal
* Modulación digital M-PAM
    * Incluye aproximación de ruido con un canal de AWGN

Al correr el programa se obtiene una imagen BMP luego de que fue codificada y decodificada.

## Requerimientos
* Python 3.6 en adelante
* Pillow 7.1.2 en adelante

## Uso
```bash
python3 simulador.py
```

ó 

```bash
python simulador.py
```

Dependiendo del alias que le tenga a python3.

## Notas
El programa tiene 3 constantes que se pueden modificar:
* M - El grado de modulación del modulador M-PAM
* Ns - Cantidad de muestras a realizar por el modulador (Ns > 20)
* Pn - Potencia del AWGN

Si el programa le tarda mucho en su computadora puede emplear el simulador con una imagen más liviana del directorio [img](img).