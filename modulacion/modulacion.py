import math
import random
from random import randint
import numpy as np
from statistics import mean

# Constantes
M = 8       # M-PAM
Ns = 25     # Definido por la frecuencia de muestreo (Ns > 20)
Pn = 0.1    # Potencia del AWGN

def modulacion_PAM(bits_a_modular):
    """ Modulacion PAM 
    Emplea un M generico modificable con la siguiente
    constelacion/asignacion bit a simbolo:
        
        0      n  n+1 n+2 n+3 n+4 n+5 n+6 n+7 n+8    M-1
    <---|---//-|---|---|---|---|---|---|---|---|-//---|--->
       -1     -7  -5  -3  -1   0   1   3   5   7      1
              --- --- --- ---     --- --- --- ---       
              M-1 M-1 M-1 M-1     M-1 M-1 M-1 M-1   

    Donde n es un número entre 0 y M-1.          
    """
    salida_amplitudes=[]
    string_bits = bits_a_modular.copy()

    # Los bits se agrupan en bloques de b bits
    b = int(math.log2(M))

    # Se anaden los ceros necesarios
    ceros = (b - len(string_bits)%b)%b
    for i in range(ceros):
        string_bits.append(0)

    # Agrupacion de bits en simbolos
    an = [string_bits[i:i+b] for i in range(0, len(string_bits), b)]

    for i in range(0, len(an)):
        # Se realiza la conversion del simbolo a su amplitud
        simbolo = ''.join(str(e) for e in an[i])
        amplitud = -1+2*int(simbolo, 2)/(M-1)

        # Muestreo
        for i in range(Ns):
            salida_amplitudes.append(amplitud)

    return salida_amplitudes, ceros

def demodulacion_PAM(string_bits, ceros):
    """ Demodulacion PAM """
    salida_bits=[]
    string_salida = ""

    # Secuencias xn(k) agrupadas de Ns en Ns
    xnk = [string_bits[i:i+Ns] for i in range(0, len(string_bits), Ns)]
    
    # Promedio de las Ns muestras para cada secuencia en xn(k)
    yn = [mean(xnk[i]) for i in range(len(xnk))]

    # Simbolos/amplitudes posibles
    ai = [-1+2*num/(M-1) for num in range(M)]

    # Elige de los ai simbolos posibles el mas cercano para las entradas de yn
    # Segun la menor distancia euclidiana a la constelacion respectiva
    an = [min(ai, key=lambda x:abs(x-yn[i])) for i in range(len(yn))]

    for i in range(len(an)):

        # Extrae los simbolos de an
        simbolo = int((M-1)/2*(an[i]+1))

        # Los bits representados por el simbolo
        bits = format(simbolo, '0' + str(int(math.log2(M))) + 'b')
        string_salida += bits

    # Se eliminan los ceros añadidos    
    if ceros != 0:
        string_salida = string_salida[:-ceros]

    salida_bits = list(string_salida) 
    salida_bits = [int(bit) for bit in salida_bits] 

    return salida_bits

def AWGN(senal):
    """
    Additive White Gaussian Noise
    """
    # Le suma el ruido gaussiano
    for i in range(len(senal)):
        senal[i] += random.gauss(0, math.sqrt(Pn))

    return senal

def main():
    """Función principal del programa
    """
    secuencia = []

    #Crea una lista de números aleatorios para que sean la entrada del codificador de canal
    for i in range(0,100):
         a = randint(0,1)
         secuencia.append(a)

    print("Se aplica", str(M) + "-PAM.")
    print("---------------------------")
    print("        MODULACION         ")
    print("---------------------------")
    print("La secuencia es: ", secuencia)
    print("La secuencia bc es de", len(secuencia), "bits.")

    #Modulacion PAM
    salida_modulador, ceros = modulacion_PAM(secuencia)

    # Descomentar si se quiere ver las amplitudes
    # print("La salida del modulador es: ", salida_modulador)

    print("La salida del modulador (x(k)) tiene", len(salida_modulador), "entradas.")
    print("Se anadieron", ceros, "ceros.")



    # #Canal ideal (Descomentar si se quiere un canal ideal)
    # entrada_demodulador = salida_modulador

    #Canal AWGN (Comentar si no se quiere un canal AWGN)
    entrada_demodulador = AWGN(salida_modulador)

    print("---------------------------")
    print("      DEMODULACION         ")
    print("---------------------------")

    #Demodulacion PAM
    salida_demodulacion = demodulacion_PAM(entrada_demodulador, ceros)

    print("La salida del demodulador es: ", salida_demodulacion)
    
    errores = 0
    for i in range(len(secuencia)):
        if secuencia[i] != salida_demodulacion[i]:
        
            errores = errores + 1

    print("La cantidad de errores es: \t", errores)

if __name__ == "__main__":
    main()
