from PIL import Image
import math
import random
import time
import numpy as np
from statistics import mean

# Constantes
M = 8       # M-PAM del modulador
Ns = 25     # Definido por la frecuencia de muestreo del modulador (Ns > 20)
Pn = 0.01    # Potencia del AWGN


def codificadorFuente(imagen):
    """ Codificador de fuente """

    # Abre la imagen
    img = Image.open(imagen)
    rgb_im = img.convert('RGB')

    # width y height de la imagen
    w, h = rgb_im.size

    # Agrega la información de cantidad de filas y columnas (en binario) al string
    string_bits = "{0:016b}".format(w) + "{0:016b}".format(h)

    for a in range (h): #Fila por fila en la matriz
        row = [rgb_im.getpixel((i,a)) for i in range(w)] # Obtiene la fila y sus elementos

        for tup in row: # Recorre cada tupla
            for item in tup: # Recorre cada elemento dentro de la tupla (R,G,B)
                string_bits = string_bits + "{0:08b}".format(item) # Agrega el número en binario al string
    return string_bits

def decodificadorFuente(string_bits):
    """ Deodificador de fuente """

    # Dimensiones de la imagen
    w = int(string_bits[:16],2)
    h = int(string_bits[16:32],2)

    # Si se quiere forzar las dimensiones de la imagen
    # Util para cuando no se transmiten las dimensiones correctamente
    #w = 512
    #h = 512

    # Bits con valores RGB de la imagen
    string_bits = string_bits[32:]

    # Divide el string en bytes y los pone en una lista
    img_int = [int(string_bits[i:i+8], 2) for i in range(0, len(string_bits), 8)]

    # Pasa la lista de bytes de tipo int a un objeto inmutable de bytes
    # que reconoce el creador de imagenes de Pillow
    img_bytes = bytes(img_int)

    # Pillow crea y guarda la imagen decodificada a partir de la informacion del objeto de bytes
    try:
        img = Image.frombytes("RGB", (w,h), img_bytes)
        img.save("imagenDecodificada.bmp")
    except ValueError:
        print("ERROR")
        print("Las dimensiones de la imagen no fueron transmitidas correctamente.")




def codificadorCanal(string_bits):
    """ Codificador de canal"""

    secuencia = list(string_bits)
    secuencia = [int(bit) for bit in secuencia]

    #En salidaCodificador se tendrá el resultado final cuando se termine la codificación de todos los conjuntos de k bits
    salidaCodificador = []
    entradaCodificador = []

    #Define la matriz generadora G
    G = [[1, 0, 0, 0, 1, 1, 0],
        [0, 1, 0, 0, 1, 0, 1],
        [0, 0, 1, 0, 0, 1, 1],
        [0, 0, 0, 1, 1, 1, 1]]

    #Límites para recorrer el arreglo
    n = 0
    m = 4

    #Guarda la cantidad de ceros agregados en la secuencia en caso de que no sea múltiplo de k = 4
    ceros = 0

    #Recorre todo el arreglo de entrada
    while n < len(secuencia):

        #Se obtienen los k = 4 bits para codificar
        entradaCodificador = secuencia[n:m]

        #Si el tamaño es menor a k (pasa solo con el último conjunto enviado) le pone ceros hasta que el tamaño sea k
        while len(entradaCodificador) < 4:
            ceros = ceros + 1
            entradaCodificador = entradaCodificador.append(0)

        #Realiza la multiplicación del vector de entrada con la matriz G y se lo adjunta al resultado final

        # Codificacion de 7 bits saliente segun la secuencia de 4 bits
        resultado = [0,0,0,0,0,0,0]

        # Multiplicacion matricial entre la secuencia de 4 bits y G
        for i in range(4):
            for j in range(7):
                resultado[j] += entradaCodificador[i] * G[i][j]

        # La codificacion es binaria
        resultado = [valor%2 for valor in resultado]


        # Se adjunta al resultado final
        salidaCodificador.extend(resultado)


        #Suma 4 a los límites para la siguiente iteración
        n += 4
        m += 4


    return (salidaCodificador,ceros)

def decodificadorCanal(salidaCodificadorCanal):
    """ Decodificador de canal"""

    #Se define la matriz H
    H = [[1, 1, 0],
        [1, 0, 1],
        [0, 1, 1],
        [1, 1, 1],
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]]

    n = 0
    m = 7

    sindromes = [0,0,0]
    entradaDecodificador = []
    salidaDecodificador = []

    #Recorre todo el arreglo de entrada del decodificador
    while n < len(salidaCodificadorCanal):

        #Se obtienen los n=7 bits para codificar
        entradaDecodificador = salidaCodificadorCanal[n:m]


        #Realiza la multiplicación del vector de entrada con la matriz H para obtener el sindrome
        # Multiplicacion matricial entre la secuencia de 4 bits y G
        for i in range(7):
            for j in range(3):
                sindromes[j] += entradaDecodificador[i] * H[i][j]

        # La decodificacion es binaria
        sindromes = [sind%2 for sind in sindromes]

        # Correcion segun el sindrome
        for i in range(7): #
            if H[i]==sindromes:
                if entradaDecodificador[i]==1:
                    entradaDecodificador[i]=0
                else:
                    entradaDecodificador[i]=1

        # Toma los ultimos 4 bits segun la matriz G
        salidaDecodificador.extend(entradaDecodificador[0:4])

        #Suma 7 a los límites para la siguiente iteración
        n = n + 7
        m = m + 7

    # El decoder de fuente recibe un string :(
    salidaDecodificadorStr = ''.join(str(bit) for bit in salidaDecodificador)

    return salidaDecodificadorStr

def BSC(entrada_bits, probabilidad):
    """ Simulacion de BSC """
    salida_bits = entrada_bits

    for i in range(len(entrada_bits)):
        if random.random() < probabilidad:
            if entrada_bits[i] == 1:
                salida_bits[i] = 0
            else:
                salida_bits[i] = 1

    return salida_bits


def modulacion_PAM(bits_a_modular):
    """ Modulacion PAM 
    Emplea un M generico modificable con la siguiente
    constelacion/asignacion bit a simbolo:
        
        0      n  n+1 n+2 n+3 n+4 n+5 n+6 n+7 n+8    M-1
    <---|---//-|---|---|---|---|---|---|---|---|-//---|--->
       -1     -7  -5  -3  -1   0   1   3   5   7      1
              --- --- --- ---     --- --- --- ---       
              M-1 M-1 M-1 M-1     M-1 M-1 M-1 M-1   

    Donde n es un número entre 0 y M-1.          
    """
    salida_amplitudes=[]
    string_bits = bits_a_modular.copy()

    # Los bits se agrupan en bloques de b bits
    b = int(math.log2(M))

    # Se anaden los ceros necesarios
    ceros = (b - len(string_bits)%b)%b
    for i in range(ceros):
        string_bits.append(0)

    # Agrupacion de bits en simbolos
    an = [string_bits[i:i+b] for i in range(0, len(string_bits), b)]

    for i in range(0, len(an)):
        # Se realiza la conversion del simbolo a su amplitud
        simbolo = ''.join(str(e) for e in an[i])
        amplitud = -1+2*int(simbolo, 2)/(M-1)

        # Muestreo
        for i in range(Ns):
            salida_amplitudes.append(amplitud)

    return salida_amplitudes, ceros

def demodulacion_PAM(string_bits, ceros):
    """ Demodulacion PAM """
    salida_bits=[]
    string_salida = ""

    # Secuencias xn(k) agrupadas de Ns en Ns
    xnk = [string_bits[i:i+Ns] for i in range(0, len(string_bits), Ns)]
    
    # Promedio de las Ns muestras para cada secuencia en xn(k)
    yn = [mean(xnk[i]) for i in range(len(xnk))]


    # Simbolos/amplitudes posibles
    ai = [-1+2*num/(M-1) for num in range(M)]

    # Elige de los ai simbolos posibles el mas cercano para las entradas de yn
    # Segun la menor distancia euclidiana a la constelacion respectiva
    an = [min(ai, key=lambda x:abs(x-yn[i])) for i in range(len(yn))]

    for i in range(len(an)):

        # Extrae los simbolos de an
        simbolo = int((M-1)/2*(an[i]+1))

        # Los bits representados por el simbolo
        bits = format(simbolo, '0' + str(int(math.log2(M))) + 'b')
        string_salida += bits

    # Se eliminan los ceros añadidos    
    if ceros != 0:
        string_salida = string_salida[:-ceros]

    salida_bits = list(string_salida) 
    salida_bits = [int(bit) for bit in salida_bits] 

    return salida_bits

def AWGN(senal):
    """
    Additive White Gaussian Noise
    """
    # Le suma el ruido gaussiano
    for i in range(len(senal)):
        senal[i] += random.gauss(0, math.sqrt(Pn))

    return senal

def main():
    """Función principal del programa
    """
    start = time.time()

    imagen = "img/LENA.bmp"
    print("-------------------------------------------")
    print("Simulacion de un sistema de comunicaciones")
    print("-------------------------------------------")

    print("El simulador puede tardar unos momentos.")
    print("Si tarda mucho en su computadora puede cambiar la imagen por una mas liviana.")
    print("Las imagenes estan disponibles en el archivo img\n")

    # Codificador de fuente
    print("Senal esta pasando por el codificador de fuente...")
    salidaCodificadorFuente = codificadorFuente(imagen)

    # Codificador de canal
    print("Senal esta pasando por el codificador de canal...")
    (salidaCodificadorCanal,cantidadCeros) = codificadorCanal(salidaCodificadorFuente)

    #Modulacion PAM
    print("Senal esta pasando por el modulador PAM...")
    salida_modulador, ceros = modulacion_PAM(salidaCodificadorCanal)

    # Canal
    # Canal ideal (Descomentar si se quiere un canal ideal)
    # entrada_demodulacion = salida_modulador

    # Canal con AWGN (Comentar si no se quiere un canal AWGN)
    entrada_demodulacion = AWGN(salida_modulador)

    #Demodulacion PAM
    print("Senal esta pasando por el demodulador PAM...")
    salida_demodulacion = demodulacion_PAM(entrada_demodulacion, ceros)

    # Decodificador de canal
    print("Senal esta pasando por el decodificador de canal...")
    salidaDecodificadorCanal = decodificadorCanal(salida_demodulacion)

    # Decodificador de fuente
    print("Senal esta pasando por el decodificador de fuente...")
    decodificadorFuente(salidaDecodificadorCanal)

    print("\nSimulacion terminada - la imagen fue guardada como imagenDecodificada.bmp")

    end = time.time()
    print("Tiempo de simulacion total: " , end - start, " s")


if __name__ == "__main__":
    main()
