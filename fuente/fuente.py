from PIL import Image


def codificadorFuente(imagen):
    """ Codificador de fuente """

    # Abre la imagen
    img = Image.open(imagen) 
    rgb_im = img.convert('RGB')

    # width y height de la imagen
    w, h = rgb_im.size

    # Agrega la información de cantidad de filas y columnas (en binario) al string
    string_bits = "{0:016b}".format(w) + "{0:016b}".format(h) 

    for a in range (h): #Fila por fila en la matriz
        row = [rgb_im.getpixel((i,a)) for i in range(w)] # Obtiene la fila y sus elementos

        for tup in row: # Recorre cada tupla
            for item in tup: # Recorre cada elemento dentro de la tupla (R,G,B)
                string_bits = string_bits + "{0:08b}".format(item) # Agrega el número en binario al string
    return string_bits

def decodificadorFuente(string_bits):
    """ Deodificador de fuente """

    # Dimensiones de la imagen
    w = int(string_bits[:16],2)
    h = int(string_bits[16:32],2)

    # Bits con valores RGB de la imagen
    string_bits = string_bits[32:]

    # Divide el string en bytes y los pone en una lista
    img_int = [int(string_bits[i:i+8], 2) for i in range(0, len(string_bits), 8)]

    # Pasa la lista de bytes de tipo int a un objeto inmutable de bytes
    # que reconoce el creador de imagenes de Pillow
    img_bytes = bytes(img_int)

    # Pillow crea y guarda la imagen decodificada a partir de la informacion del objeto de bytes
    img = Image.frombytes("RGB", (w,h), img_bytes)
    img.save("imagenDecodificada.bmp")


def main():
    """Función principal del programa
    """
    imagen = "../img/lena.bmp"
    # Codificador
    salidaCodificadorFuente = codificadorFuente(imagen)

    # Descomentar si quiere ver la salida del codificador
    # print(salidaCodificadorFuente)

    #Simula el canal ideal al copiar todos los bits de salida a la secuencia de bits recibidos
    entradaDecodificadorFuente = salidaCodificadorFuente

    # Decodificador
    decodificadorFuente(entradaDecodificadorFuente)


if __name__ == "__main__":
    main()
